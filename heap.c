#include<stdio.h>
#include<stdlib.h>
#define MAX 7

main() {
    int vetor[MAX] = {22, 13, 14, 80, 55, 9, 12};
    int i, pai, filho, aux, n;

    i = MAX/2;
    n = MAX;
    while(1) {
        if (i > 0) {
            i--;
            aux = vetor[i];
        } else {
            n--;
            if (n == 0)
                break;
            aux = vetor[n];
            vetor[n] = vetor[0];
        }
        pai = i;
        filho = (i*2) + 1;
        while (filho < n) {
            if ((filho + 1 < n) && (vetor[filho + 1] > vetor[filho]))
                filho++;
            if (vetor[filho] > aux) {
                vetor[pai] = vetor[filho];
                pai = filho;
                filho = (pai*2) + 1;
            } else
                break;
        }
        vetor[pai] = aux;
    }
    for (i=0;i<MAX;i++) {
        printf("%d ", vetor[i]);
    }
    printf("\n");
}