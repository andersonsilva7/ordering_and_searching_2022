#include<stdio.h>
#define QTDE 8

main() {
    int vet[QTDE] = {23,60,18,11,3,88,1,2};
    int aux, i;

    for (int j=1; j< QTDE; j++) {
        aux = vet[j];
        i = j-1;
        while((i>=0) && (vet[i]>aux)) {
            vet[i+1] = vet[i];
            i--;
        }
        vet[i+1] = aux;
    }
    for (aux=0; aux< QTDE; aux++) {
        printf("%d ", vet[aux]);
    }
    printf("\n");

    //se o vetor já estiver ordenado, o custo dele é de O(n)
    // o pior caso, vai ser O(n²)
    // é um metdo estavel
}